import {Validators} from "@angular/forms";
import {CreditCardValidator} from "angular-cc-library";

export const VALIDACION_NOMBRE = [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(40),
];

export const MENSAJES_VALIDACION_NOMBRE = {
    required: 'El campo nombre es obligatorio',
    minlength: 'El campo nombre debe tener mínimo 3 caracteres',
    maxlength: 'El campo nombre no debe tener más de 40 caracteres'
};


export const VALIDACION_CARD_NUMBER = [
    Validators.minLength(3),
    Validators.maxLength(40),
    Validators.required,
    CreditCardValidator.validateCCNumber
];

export const MENSAJES_VALIDACION_CARD_NUMBER = {
    required: 'El campo nombre es obligatorio',
    minlength: 'El campo nombre debe tener mínimo 3 caracteres',
    maxlength: 'El campo nombre no debe tener más de 40 caracteres'
};

export const VALIDACION_EXP_DATE = [
    Validators.required,
    CreditCardValidator.validateExpDate
];

export const MENSAJES_VALIDACION_EXP_DATE= {
    required: 'El campo nombre es obligatorio',
};

export const VALIDACION_CVC = [
    Validators.required,
];

export const MENSAJES_VALIDACION_CVC= {
    required: 'El campo nombre es obligatorio',
};