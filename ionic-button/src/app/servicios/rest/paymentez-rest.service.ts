import {Injectable} from '@angular/core';
import {HTTP} from '@ionic-native/http/ngx';

@Injectable()
export class PaymentezRestService {
    private url = 'http://192.168.0.103:8080/paymentez';
    constructor(
        private readonly http: HTTP
    ) {}

    addTarjeta(tarjeta: any) {
        return new Promise(async (resolve, reject) => {
            try {
                const url = this.url  + '/add-tarjeta';
                const request = await this.http
                    .post(
                        url,
                        tarjeta, {});
                const respuesta = request.data;
                resolve(respuesta);
            } catch (e) {
                console.log('Error añadiendo tarjeta', e);
                reject(e);
            }
        });
    }

    listarTarjeta() {
        return new Promise(async (resolve, reject) => {
            try {
                const url = this.url  + '/tarjetas';
                const request = await this.http
                    .get(url,{},{});
                const respuesta = request.data;
                resolve(respuesta);
            } catch (e) {
                console.log('Error añadiendo tarjeta', e);
                reject(e);
            }
        });
    }
}
