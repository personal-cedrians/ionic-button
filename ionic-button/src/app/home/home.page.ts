import { Component } from '@angular/core';
import {PaymentezRestService} from '../servicios/rest/paymentez-rest.service';
import {ToastController} from "@ionic/angular";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  formularioValido;
  tarjetaAIngresar;

  constructor(
      private readonly _paymentezRestService: PaymentezRestService,
      public toastController: ToastController
  ) {}

  validarFormulario(tarjetaValida) {
    if (tarjetaValida) {
      this.tarjetaAIngresar = tarjetaValida;
      this.formularioValido = true;
    } else {
      this.formularioValido = false;
    }
  }

  async onSubmit() {
    try{
      await this._paymentezRestService.addTarjeta(this.tarjetaAIngresar)
      const toast = await this.toastController.create({
        message: 'Todo ok bro',
        duration: 2000,
        color: "success",
      });
      toast.present();
    }catch (e) {
        const toast = await this.toastController.create({
          message: 'Error creando tarjeta',
          duration: 2000,
          color: "danger",
        });
        toast.present();
      }
    }
}
