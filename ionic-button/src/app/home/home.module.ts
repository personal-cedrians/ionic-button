import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import {CreditCardDirectivesModule} from 'angular-cc-library';
import {CardFormComponent} from '../card/card-form/card-form.component';
import {PaymentezRestService} from '../servicios/rest/paymentez-rest.service';
import {CardListComponent} from "../card/card-list/card-list.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreditCardDirectivesModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    ReactiveFormsModule,
  ],
  declarations: [
      HomePage,
     CardFormComponent,
      CardListComponent,
  ],
  providers: [

  ]
})
export class HomePageModule {}
