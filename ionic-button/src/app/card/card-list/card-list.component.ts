import { Component, OnInit } from '@angular/core';
import {PaymentezRestService} from "../../servicios/rest/paymentez-rest.service";
import {ToastController} from "@ionic/angular";

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss'],
})
export class CardListComponent implements OnInit {
  tarjetas;
  token;

  constructor(
      private readonly _paymentezRestService: PaymentezRestService,
      public toastController: ToastController
  ) { }

  async ngOnInit() {
    try {
      const respuesta = await this._paymentezRestService.listarTarjeta() as string;
      this.tarjetas = JSON.parse(respuesta);
    }catch (e) {
      const toast = await this.toastController.create({
        message: 'No se cargaron las tarjetas',
        duration: 2000,
        color: "danger",
      });
      toast.present();
    }

  }

  cambioRadio(evento){
    console.log(evento.detail.value);
  }

}
