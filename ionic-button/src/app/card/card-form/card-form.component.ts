import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../funciones/generar-mensajes-error';
import {
    MENSAJES_VALIDACION_CARD_NUMBER, MENSAJES_VALIDACION_CVC, MENSAJES_VALIDACION_EXP_DATE, MENSAJES_VALIDACION_NOMBRE,
    VALIDACION_CARD_NUMBER, VALIDACION_CVC,
    VALIDACION_EXP_DATE, VALIDACION_NOMBRE
} from '../../funciones/validaciones/validacion-card';
import {PaymentezRestService} from '../../servicios/rest/paymentez-rest.service';

@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
  styleUrls: ['./card-form.component.scss'],
})
export class CardFormComponent implements OnInit {
  @Output() cardValida: EventEmitter<any | boolean> = new EventEmitter();
  cardForm: FormGroup;
  mensajesError = {
    creditCard: [],
  };
  subscribers = [];


  constructor(
      private readonly _formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this._inicializarFormulario();
    this._verificarCamposFormulario();
    this._verificarFormulario();
  }

  private _inicializarFormulario() {
    this.cardForm = this._formBuilder.group({
        nombreCliente: ['', VALIDACION_NOMBRE],
        creditCard: ['', VALIDACION_CARD_NUMBER],
        expiration: ['', VALIDACION_EXP_DATE],
        cvc: ['', VALIDACION_CVC],
    });
  }

  private _verificarCamposFormulario() {
      this.verificarCampoFormControl('nombreCliente', MENSAJES_VALIDACION_NOMBRE);
      this.verificarCampoFormControl('creditCard', MENSAJES_VALIDACION_CARD_NUMBER);
      this.verificarCampoFormControl('expiration', MENSAJES_VALIDACION_EXP_DATE);
      this.verificarCampoFormControl('cvc', MENSAJES_VALIDACION_CVC);
  }

  private _verificarFormulario() {
    const formularioFormGroup = this.cardForm;
    const subscriber = formularioFormGroup
        .valueChanges
        .subscribe(
            formulario => {
              const cardValida = formularioFormGroup.valid;
              if (cardValida) {
                this.cardValida.emit(formulario);
              } else {
                this.cardValida.emit(false);
              }
            }
        );
    this.subscribers.push(subscriber);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.cardForm.get(campo);
    const subscriber = campoFormControl
        .valueChanges
        .pipe(debounceTime(500))
        .subscribe(
            valor => {
              console.log(valor);
              this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
            }
        );
    this.subscribers.push(subscriber);
  }

  onSubmit(a){
      console.log(a)
  }
}
